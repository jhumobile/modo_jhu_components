<?php

/*
 * Copyright © 2010 - 2014 Modo Labs Inc. All rights reserved.
 *
 * The license governing the contents of this file is located in the LICENSE
 * file located at the root directory of this distribution. If the LICENSE file
 * is missing, please contact sales@modolabs.com.
 *
 */

class JHUAuthenticatedPeopleModule extends KGOModule {
    protected static $feedModelClass = 'KGOPeopleDataModel';
    const MODULE_AUTHORITY = 's-auth-shib';

    protected function initializeForPage_index(KGOUIPage $page, $objects=null) {
        if($this->isLoggedIn(self::MODULE_AUTHORITY)) {
            $user = $this->getUser(self::MODULE_AUTHORITY);
        }

        if(isset($user)) {
            $pageConfig = $this->getConfig('page-authenticated.objdefs.content.default');
        } else {
            $page->setRegionContents('content', array());
            $pageConfig = $this->getConfig('page-unauthenticated.objdefs.content.default');
        }
        $this->addObjectDefinitionsToPage($page, $pageConfig, $this, $this, true);
    }

    protected function initializeForPageConfigObjects_index(KGOUIPage $page, $objects) {
    }

    protected function initializeForPageConfigObjects_search(KGOUIPage $page, $objects) {
        if (!($feed = $this->getFeed())) {
            $this->setPageError($page, 'jhu_authenticated_people.errors.feedNotConfigured');
            return;
        }
    }

    protected function initializeForPageConfigObjects_detail(KGOUIPage $page, $objects) {
        if (!($feed = $this->getFeed())) {
            $this->setPageError($page, 'modo_people.errors.feedNotConfigured');
            return;
        }
    }

    protected function initializeForPageConfigObjects_bookmarks(KGOUIPage $page, $objects) {
        if (!($feed = $this->getFeed())) {
            $this->setPageError($page, 'modo_people.errors.feedNotConfigured');
            return;
        }
    }
}
