<?php

/*
 * Copyright © 2010 - 2014 Modo Labs Inc. All rights reserved.
 *
 * The license governing the contents of this file is located in the LICENSE
 * file located at the root directory of this distribution. If the LICENSE file
 * is missing, please contact sales@modolabs.com.
 *
 */

class ModoLegacyUpgradeModule extends KGOModule {
    const CLIENT_TYPE_ANDROID = 'compliant-android-native';
    const CLIENT_TYPE_ANDROID_TABLET = 'tablet-android-native';
    const CLIENT_TYPE_IOS = 'compliant-iphone-native';

    private $upgradeVersion;
    private $upgradeURL;

    protected function initializeForPageFallback(KGOUIPage $page, $command) {

        $clientType = $this->getArg("client", self::CLIENT_TYPE_IOS);

        if($clientType == self::CLIENT_TYPE_ANDROID || $clientType == self::CLIENT_TYPE_ANDROID_TABLET) {
            $this->upgradeURL = $this->getConfig("module.options.androidURL");
            $this->upgradeVersion = $this->getConfig("module.options.androidUpgradeVersion");
        }
        else if($clientType == self::CLIENT_TYPE_IOS) {
            $this->upgradeURL = $this->getConfig("module.options.iosURL");
            $this->upgradeVersion = $this->getConfig("module.options.iosUpgradeVersion");
        } else {
            echo $this->createUpgradeAPIError();
            die(0);
        }

        echo $this->createUpgradeAPIResponse();
        die(0);

    }

    private function createUpgradeAPIResponse() {
        $response = array(
            "id" => "kurogo",
            "tag" => "kurogo",
            "command" => "hello",
            "version" => 0,
            "error" => array(
                "code" => 7,
                "title" => $this->getConfig("module.options.upgradeTitle"),
                "message" => $this->getConfig("module.options.upgradeMessage"),
                "data" => array(
                    "url" => $this->upgradeURL,
                    "version" => $this->upgradeVersion
                )
            ),
            "warnings" => null,
            "response" => (object)array(),
            "context" => null,
            "contexts" => array()
        );

        return json_encode($response);
    }

    private function createUpgradeAPIError() {
        $response = array(
            "id" => "",
            "tag" => null,
            "command" => null,
            "version" => 0,
            "error" => array(
                "code" => 0,
                "title" => "Error",
                "message" => "An error has occured",
                "data" => null
            ),
            "warnings" => null,
            "response" => (object)array(),
            "context" => null,
            "contexts" => array()
        );

        return json_encode($response);
    }
}