<?php

/*
 * Copyright © 2010 - 2014 Modo Labs Inc. All rights reserved.
 *
 * The license governing the contents of this file is located in the LICENSE
 * file located at the root directory of this distribution. If the LICENSE file
 * is missing, please contact sales@modolabs.com.
 *
 */

/**
 * @ingroup Calendar_Model DataParser
 *
 * @brief The ICal* classes in this file together partially implement RFC 2445.
 */

class JHUHUBtoICSDataParser extends KGODataParser
{
    protected static $nonRepeatingValues = array(
        // from http://tools.ietf.org/rfc/rfc5545.txt
        'ACTION',       // Alarm
        'CALSCALE',     // Calendar
        'CLASS',        // Event, To-Do, Journal
        'COMPLETED',    // To-Do
        'CREATED',      // Event, To-Do, Journal
        'DESCRIPTION',  // Event, To-Do, Alarm (Note: can be multi-valued for Journal)
        'DTEND',        // Time Zone
        'DTSTAMP',      // Event, To-Do, Journal
        'DTSTART',      // To-Do, Journal, Free/Busy
        'DURATION',     // Alarm
        'GEO',          // Event, To-Do
        'LAST-MOD',     // Event, To-Do, Journal, Time Zone
        'LOCATION',     // Event, To-Do
        'METHOD',       // Calendar
        'ORGANIZER',    // Event, To-Do, Journal, Free/Busy
        'PERCENT',      // To-Do
        'PRIORITY',     // Event, To-Do
        'PRODID',       // Calendar
        'RECURID',      // Event, To-Do, Journal
        'REPEAT',       // Alarm
        'SEQ',          // Event, To-Do, Journal
        'STATUS',       // Event, To-Do, Journal
        'SUMMARY',      // Event, To-Do, Journal, Alarm
        'TRANSP',       // Event
        'TRIGGER',      // Alarm
        'TZID',         // Time Zone
        'TZOFFSETFROM', // Time Zone
        'TZOFFSETTO',   // Time Zone
        'TZURL',        // Time Zone
        'UID',          // Event, To-Do, Journal, Free/Busy
        'URL',          // Event, To-Do, Journal, Free/Busy
        'VERSION',      // Calendar

        // TODO:  Make this more sophisticated because some keys repeat for some types and not others.
        // Currently we only generate event objects so that's the behavior we use.
        //
        // ATTACH - Single-valued for Alarm, but multi-valued for Event
        // DESCRIPTION - Single-valued for Event, To-Do and Alarm, but multi-valued for Journal
        // CONTACT - Single-valued for Free/Busy, but multi-valued for Event, To-Do and Journal
    );

    protected $parseMap = array(
        'class' => 'KGOICSCalendarEvent',
        'key' => 'VCALENDAR.VEVENT',
        'prefix' => 'ics',
        'includeUnmappedAttributes' => true,
        'attributes' => array(
            KGODataObject::ID_ATTRIBUTE             => 'UID',
            KGODataObject::TITLE_ATTRIBUTE          => 'SUMMARY',
            KGODataObject::DESCRIPTION_ATTRIBUTE    => 'DESCRIPTION',
            KGOCalendarEvent::START_ATTRIBUTE       => 'DTSTART',
            KGOCalendarEvent::END_ATTRIBUTE         => 'DTEND',
            KGOICSCalendarEvent::DURATION_ATTRIBUTE => 'DURATION',
            KGOCalendarEvent::URL_ATTRIBUTE         => 'URL',
            KGOCalendarEvent::LOCATION_ATTRIBUTE    => 'LOCATION',
        ),
        'processors' => array(
            'ics:DTSTAMP' => array(
                array(
                    'class' => 'KGOICSDateTimeDataProcessor'
                )
            ),
            KGOICSCalendarEvent::EXCEPTION_DATE_ATTRIBUTE => array(
                array(
                    'class' => 'KGOICSDateTimeDataProcessor'
                )
            ),
            KGOICSCalendarEvent::DURATION_ATTRIBUTE => array(
                array(
                    'class' => 'KGOICSDurationDataProcessor'
                )
            ),
            KGOCalendarEvent::START_ATTRIBUTE => array(
                array(
                    'class' => 'KGOICSDateTimeDataProcessor'
                )
            ),
            KGOCalendarEvent::END_ATTRIBUTE => array(
                array(
                    'class' => 'KGOICSDateTimeDataProcessor'
                )
            ),
            KGOCalendarEvent::LOCATION_ATTRIBUTE => array(
                array(
                    'class' => 'KGOTrimToNullDataProcessor'
                )
            ),
        ),
    );

    private static function escapeText($text) {
        $text = str_replace(array("\"","\\",",",";","\n"), array("DQUOTE","\\\\", "\,","\;","\\n"), $text);
        return $text;
    }

    private static function unescapeText($text) {
        $text = str_replace(array("DQUOTE","\\\\", "\,","\;","\\n","\\r"), array("\"","\\",",",";","\n",""), $text);
        return $text;
    }

    protected function parseLine($line) {
        // NAME:VALUE;PARAMS
        if (preg_match('/([^":]*(?:"[^"]*"[^":]*)*):(.*)/', $line, $parts)) {
            $params = explode(';', $parts[1]);

            $lineData = array(
                'name' => array_shift($params),
                'value' => trim(self::unescapeText($parts[2])),
                'params' => array(),
            );

            foreach ($params as $param) {
                if (preg_match("/(.*?)=(.*)/", $param, $param_bits)) {
                    $lineData['params'][$param_bits[1]] = str_replace("\"", "", $param_bits[2]);
                }
            }
        } else {
            $lineData = null;
        }

        return $lineData;
    }

    protected function parseValue($name, $value, $params) {
        switch ($name) {
            case 'DTSTART':
            case 'DTEND':
            case 'DTSTAMP':
                if (isset($params['TZID'])) {
                    $value = $value . '|' . $params['TZID'];
                }
        }

        return $value;
    }
    public function parseMonth($textMonth)
    {
	switch($textMonth)
	{
	    case "January":
		$month = "01";
		break;
	    case "February":
		$month = "02";
		break;
	    case "March":
		$month = "03";
		break;
	    case "April":
		$month = "04";
		break;
	    case "May":
		$month = "05";
		break;
	    case "June":
		$month = "06";
		break;
	    case "July":
		$month = "07";
		break;
	    case "August":
		$month = "08";
		break;
	    case "September":
		$month = "09";
		break;
	    case "October":
		$month = "10";
		break;
	    case "November":
		$month = "11";
		break;
	    case "December":
		$month = "12";
		break;
	}
	return $month;
    }
    
    public function parseWhen($datetime)
    {
	// Year is being ignored at this time as we anticipate having a proper iCal feed before it would come up
	$inDT = explode(' ',$datetime);
	//Possible formats:
	// Type: 4 - []StartMonth[]StartDateMK[]StartTimeSM
	// Type: 6 - []StartMonth[]StartDateMK[]StartTimeSM[]-[]EndTimeEM
	// Type: 6B - []StartMonth[]StartDateMK[]-[]StartMonth[]EndDateMK
        // Type: 7 - []StartMonth[]StartDateMK[]StartTimeSM[]-[]EndMonth[]EndDateMK
	// Type: 8 - []StartMonth[]StartDateMK[]StartTimeSM[]-[]EndMonth[]EndDateMK[]EndTimeEM
        if ($inDT[3]=='-') //6B is invalid, convert to 7
	    $inDT = array($inDT[0],$inDT[1],$inDT[2],'12:00PM',$inDT[3],$inDT[4],$inDT[5],'');
        
	$dateType = count($inDT);	             

	$startMonth = $this->parseMonth($inDT[1]);
	$startDate = substr($inDT[2],0,strlen($inDT[2])-2);
	
	$startTimeRaw = ($dateType > 4) ? $inDT[3] : substr($inDT[3],0,strpos($inDT[3],"<"));
	$startTimeClock = str_replace(':','',substr($startTimeRaw,0,strlen($startTimeRaw)-2));
	$startTimeMP = substr($startTimeRaw,strlen($startTimeRaw)-2,1); 
	$startTime = ($startTimeClock/100 >= 12 && $startTimeClock/100 < 13)
		    ? (($startTimeMP=='A' || $startTimeMP=='a') ? 0 : 1200)
		    : $startTimeClock + (($startTimeMP=='A' || $startTimeMP=='a') ? 0 : 1200);
	
	$endMonth = $startMonth;
	$endDate = $startDate;
	$endTime = $startTime+100;
	
	if ($dateType > 4)
	{
	    $eTi = $dateType-1;//endTime index
	    if ($dateType > 6)
	    {
		$endMonth = $this->parseMonth($inDT[5]);
		$endDate = substr($inDT[6],0,strlen($inDT[6])-2);
	    }
	    $endTimeRaw = str_replace(':','',substr($inDT[$eTi],0,strlen($inDT[$eTi])-2));
	    $endTimeMP = substr($inDT[$eTi],strlen($inDT[$eTi])-2,1); 
	    $endTime = $endTimeRaw + (($endTimeMP=='A' || $endTimeMP=='a') ? 0 : 1200);	 
	    if ($endMonth==$startMonth && $endDate==$startDate && $endTime<=$startTime)
		$endTime = $startTime+100;
	}
	if ($startTime<1000)
	    $startTime = '0'.$startTime;
	if ($endTime<1000)
	    $endTime = '0'.$endTime;
	if ($startDate<10)
	    $startDate= '0'.$startDate;
	if ($endDate<10)
	    $endDate= '0'.$endDate;
	        
	
	//DTEND: datetime the event ends, 4 digit year 2 digit month 2 digit day 'T' 24hr 2 digit hour 2 digit minute 2 digit second 'Z'
	//DTSTAMP: datetime the event info was accessed, 4 digit year 2 digit month 2 digit day 'T' 24hr 2 digit hour 2 digit minute 2 digit second 'Z'
	//DTSTART: datetime the event starts, 4 digit year 2 digit month 2 digit day 'T' 24hr 2 digit hour 2 digit minute 2 digit second 'Z'
	$outEND = date("Y").$endMonth.$endDate.'T'.$endTime.'00';
	$outSTAMP = date("Ymd\THis");
	$outSTART = date("Y").$startMonth.$startDate.'T'.$startTime.'00';
		
	return array('DTEND'=>$outEND,'DTSTAMP'=>$outSTAMP,'DTSTART'=>$outSTART);
    }
        
    public function rsstoical($contents)
    {
	$contents=str_replace('&lt;em&gt;','',str_replace('&lt;/em&gt;','',$contents));
        $contents=preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $contents);
	$xmlContents = array(simplexml_load_string($contents));
	$glue='\n';
	$iCalClose="END:VCALENDAR";
	$iCalEventClose="END:VEVENT";
	$iCalEventOpen="BEGIN:VEVENT";
	$iCalOpen="BEGIN:VCALENDAR".$glue."VERSION:2.0".$glue."X-WR-CALNAME:Johns Hopkins University Calendar".$glue."CALSCALE:GREGORIAN".$glue."PRODID:iCalendar-Ruby";
	$iCalEvents=Array();
	$iCalEventsStrings=Array();
	$disclaimer = 'Please note that all event information in this section comes from HUB Events. Please be sure to verify any event information through the included hub.jhu.edu link. ';
	foreach($xmlContents[0]->channel->item as $event)
	{
	    if(isset($event->description))
	    {
		$locateWhen=strpos($event->description,'When');
		if ($locateWhen>0)
		{
		    //empirically determined numbers of irrelevant HTML characters to remove
		    $offsetWhen     = 5;
		    $offsetWhere    = 18;
		    
		    $findWhen   = array(strpos($event->description,':',$locateWhen)+1,strpos($event->description,'<br')-$offsetWhen);
		    $parsedWhen = $this->parseWhen(substr($event->description,$findWhen[0],$findWhen[1]));
		        
		    $locateWhere = strpos($event->description,'Where:',$findWhen[1]);
		    $containsWhere = $locateWhere>0;
		    $findWhere     = $containsWhere ? array(strpos($event->description,':',$findWhen[1]+$offsetWhen)+1,strpos($event->description,'</p>')) : 0;
		    $parsedWhere   = $containsWhere ? substr($event->description,$findWhere[0],$findWhere[1]-$findWhere[0]) : 'NONE';
		    
		    $findDesc   = ($containsWhere ? $findWhere[1]: $findWhen[1]);
		    $parsedDesc = trim(preg_replace('/\r\n|\r|\n/', ' ',str_replace('<p>','',str_replace('</p>','',$disclaimer.substr($event->description,$findDesc)))));
		    $iCalEvents[] = array(
	    			  'DESCRIPTION'=>"DESCRIPTION:".$parsedDesc,
	    			  'DTEND'=>"DTEND:".$parsedWhen['DTEND'],
	    			  'DTSTAMP'=>"DTSTAMP:".$parsedWhen['DTSTAMP'],
	    			  'DTSTART'=>"DTSTART:".$parsedWhen['DTSTART'],
	    			  'LOCATION'=>"LOCATION:".$parsedWhere,
	    			  'SEQUENCE'=>"SEQUENCE:0",
				  'SUMMARY'=>"SUMMARY:".$event->title,
				  'UID'=>"UID:JHUMRSS-".md5($parsedWhen['DTSTART'])."-".md5($event->title),
				  'URL'=>"URL:".$event->link
				);    
		}
	    }
	}
	foreach($iCalEvents as $iCalEvent)
	    $iCalEventStrings[] = $iCalEventOpen.$glue.implode('\n',$iCalEvent).$glue.$iCalEventClose;
	//print_r($iCalOpen.$glue.implode('',$iCalEventStrings).$glue.$iCalClose);
	//die();	
	return $iCalOpen.$glue.implode($glue,$iCalEventStrings).$glue.$iCalClose;
    }
    
    public function parseData($data) {
        $parsedData = array();
        $stack = array();

        $isRSS = strpos($data,'BEGIN:VCALENDAR')==0 && strpos($data,'<rss version')>0;
	if ($isRSS)
	    $data=$this->rsstoical($data);
        //print(nl2br(str_replace("<","&lt",$data)));
	//die();
        $newlineChar = ($isRSS ? '\n' : "\n");
        // all leading and trailing whitespace will be ignored, unfold newline space
        $data = str_replace(array("\r\n", "\n "), array("\n", ""), trim($data));
        $lines = explode($newlineChar, $data);
        $element = null;

        foreach ($lines as $line) {
            if (!($lineData = $this->parseLine($line))) {
                continue;
            }
            $name = $lineData['name'];
            $value = $lineData['value'];
            $params = $lineData['params'];

            switch($name) {
                case 'BEGIN':
                    // start a new element. If there already is an element, add it to the stack
                    if ($element){
                        $stack[] = $element;
                    }
                    $element = array('name'=> $value, 'values'=> array());
                    break;

                case 'END':
                    // check to see if the END matches with a BEGIN
                    if ($element['name'] != $value) {
                        throw new KGODataException("BEGIN $elementName ended by END $value");
                    }

                    // get the parent
                    if ($parent = array_pop($stack)) {

                        // check to see if this is a non-repeating element
                        if (!in_array($element['name'], static::$nonRepeatingValues)) {
                            $parent['values'][$element['name']][] = $element['values'];

                        } elseif (isset($parent['values'][$element['name']])) {
                            // This element occurs more than one, but on the list of non-repeating elements.
                            // If you get this error from a real feed, file a feature request.
                            kgo_flash_debug(get_class($this)." skipping unexpected repeated property '{$element['name']}'");

                        } else {
                            // add it to the list of values for the parent
                            $parent['values'][$element['name']] = $element['values'];
                        }

                        $element = $parent;

                    } else {
                        // We're in the first element of the stack
                        $parsedData[$element['name']] = $element['values'];
                    }
                    break;

                default:
                    // This is a regular value
                    // check to see if this is a non-repeating element
                    if (!in_array($name, static::$nonRepeatingValues)) {
                        $element['values'][$name][] = $this->parseValue($name, $value, $params);

                    } elseif (isset($element['values'][$name])) {
                        // This element occurs more than one, but on the list of non-repeating elements.
                        // If you get this error from a real feed, file a feature request.
                        kgo_flash_debug(get_class($this)." skipping unexpected repeated property '$name'");

                    } else {
                        // add it to the list of values for this element
                        $element['values'][$name] = $this->parseValue($name, $value, $params);
                    }
                    break;
            }
        }

        return $parsedData;
    }
}

