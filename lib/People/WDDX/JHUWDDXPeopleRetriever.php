<?php

class JHUWDDXPeopleRetriever extends KGOURLDataRetriever implements KGOSearchDataRetriever, KGOItemDataRetriever
{
    protected static $defaultParserClass = 'JHUWDDXPeopleParser';
    protected $webSvcKey;
    protected $svcParamSet;
    protected $svcParamVal;
    protected $encryptionKey;
    
    public function init($args)
    {
        parent::init($args);
        if(isset($args['serviceParameterSetting']) && isset($args['serviceParameterValue']))
        {
            $this->setSvcParamSetVal($args['serviceParameterSetting'],$args['serviceParameterValue']);
        }
        
        if(isset($args['webServiceKey']))
        {
            $this->setWebSvcKey($args['webServiceKey']);
        }
        
        if(isset($args['encryptionKey']))
        {
            $this->setEncryptionKey($args['encryptionKey']);
        }        
    }
    
    public function getSvcParamSet()
    {    
        return $this->svcParamSet;   
    }
    
    public function getSvcParamVal()
    {    
        return $this->svcParamVal;   
    }
    
    public function getWebSvcKey()
    {    
        return $this->webSvcKey;   
    }
    
    public function getEncryptionKey()
    {    
        return $this->encryptionKey;   
    }
    
    protected function setSvcParamSetVal($set,$val)
    {
        $this->addParameter($set, $val);
        $this->svcParamSet = $set;
        $this->svcParamVal = $val;
    }
    protected function setWebSvcKey($value)
    {
        $this->addParameter('webservicekey', $value);
        $this->webSvcKey = $value;
    }

    protected function setEncryptionKey($value)
    {
        $this->encryptionKey = $value;
    }
        
    public function search($searchTerms, &$response = null)
    {
        $this->addParameter('criteria', $this->encrypt($searchTerms));
        $this->setParsemap(self::WDDXPersonParsemap());

        $results = $this->getData($response);
        //kgo_debug($results, true, true);
        return $results;
    }

    /**
     * Get an item.
     *
     * @param  string $id                     The item id.
     * @param  KGODataResponse|null $response The response object.  Used if calling getData().
     * @return KGODataObject|null             The item or null if it does not exist.
     */
    public function getItem($id, &$response=null) {
        $this->addParameter('criteria', $this->encrypt($id));
        $this->setParsemap(self::WDDXPersonParsemap());

        $results = $this->getData($response);
        //kgo_debug($results, true, true);
        return current($results);
    }

    /**
     * Get an array of items.
     *
     * @param  KGODataResponse|null $response The response object.  Used if calling getData().
     * @return array                          An array of KGODataObjects.
     */
    public function getItems(&$response=null) {
        kgo_debug("getItems", true, true);
    }

    protected function encrypt($string)
    {
        // Because PHP doesn't have PKCS#12 padding support
        $pad = 16 - (strlen($string) % 16);
        if ($pad > 0) {
            $string .= str_repeat(chr($pad), $pad);
        }
        return bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, base64_decode("MYJHMOBILEKEYPLEASE111="), $string, MCRYPT_MODE_ECB));
    }

    private static function WDDXPersonParsemap() {
        return array(
            'key'        => 'COLLECTION',
            'array'      => true,
            'includeUnmappedAttributes' => true,
            'prefix' => "jhu",
            'class'      => 'KGOPerson',
            'attributes' => array(
                KGODataObject::ID_ATTRIBUTE    => 'uid',
                KGODataObject::TITLE_ATTRIBUTE => 'cn',
                KGOPerson::LASTNAME_ATTRIBUTE => 'sn',
                KGOPerson::NAME_ATTRIBUTE => 'cn',
            ),
            'processors' => array (
                KGOPerson::EMAIL_ATTRIBUTE => array(
                   array(
                       'class' => 'KGOAlternateDataProcessor',
                       'options' => array(
                            'args' => array(
                                       'jhu:mail','jhu:johnshopkinseduemaildisplay','jhu:jheUNIVemaildisplay',
                                       'jhu:jheJHHemaildisplay','jhu:jheHOSPemaildisplay','jhu:jheISISemaildisplay',
                                       'jhu:jheAlumniemaildisplay','jhu:jheADHOCemaildisplay','jhu:jheAPLemaildisplay',
                                       'jhu:jheACHemaildisplay','jhu:jheMSOemaildisplay','jhu:jheKKIemaildisplay',
                                       'jhu:jheSOMemaildisplay','jhu:jheSIBLEYemaildisplay','jhu:jheINTRASTAFFemaildisplay',
                                       'jhu:jheSUBURBANemaildisplay','jhu:jheSingaporeemaildisplay','jhu:jheHHMIemaildisplay',
                                       'jhu:jheHCGHemaildisplay'
                            )
                       )
                   )
                ),
                KGOPerson::PHONE_ATTRIBUTE => array(
                   array(
                       'class' => 'KGOAlternateDataProcessor',
                       'options' => array(
                            'args' => array(
                                        'jhu:telephonenumber','jhu:johnshopkinseduprivateofficephone',
                                        'jhu:homephone','jhu:mobile',
                                        'jhu:johnshopkinsedualtph','jhu:johnshopkinsedualtph',
                                        'jhu:jheUNIVtelephonenumber','jhu:jheUNIVprivateofficephone',
                                        'jhu:jheJHHtelephonenumber','jhu:jheJHHprivateofficephone',
                                        'jhu:jheHOSPtelephonenumber','jhu:jheHOSPprivateofficephone',
                                        'jhu:jheISIStelephonenumber','jhu:jheISISprivateofficephone',
                                        'jhu:jheAlumnitelephonenumber','jhu:jheAlumniprivateofficephone',
                                        'jhu:jheADHOCtelephonenumber','jhu:jheADHOCprivateofficephone',
                                        'jhu:jheAPLtelephonenumber','jhu:jheAPLprivateofficephone',
                                        'jhu:jheACHtelephonenumber','jhu:jheACHprivateofficephone',
                                        'jhu:jheMSOtelephonenumber','jhu:jheMSOprivateofficephone',
                                        'jhu:jheKKItelephonenumber','jhu:jheKKIprivateofficephone',
                                        'jhu:jheSOMtelephonenumber','jhu:jheSOMprivateofficephone',
                                        'jhu:jheSIBLEYtelephonenumber','jhu:jheSIBLEYprivateofficephone',
                                        'jhu:jheINTRASTAFFtelephonenumber','jhu:jheINTRASTAFFprivateofficephone',
                                        'jhu:jheSUBURBANtelephonenumber','jhu:jheSUBURBANprivateofficephone',
                                        'jhu:jheSingaporetelephonenumber','jhu:jheSingaporeprivateofficephone',
                                        'jhu:jheHHMItelephonenumber','jhu:jheHHMIprivateofficephone',
                                        'jhu:jheHCGHtelephonenumber','jhu:jheHCGHprivateofficephone',
                            )
                        )
                   )
                ),
                KGOPerson::FIRSTNAME_ATTRIBUTE => array(
                   array('class' => 'KGOAlternateDataProcessor','options' => array('args' => array('jhu:displayname','jhu:givenname','jhu:fn')))
                ),
                KGOPerson::TITLE_ATTRIBUTE => array(
                   array(
                       'class' => 'KGOAlternateDataProcessor',
                       'options' => array(
                            'args' => array(
                                'jhu:title','jhu:jheUNIVtitle',
                                'jhu:jheAlumnititle','jhu:jheISIStitle',
                                'jhu:jheJHHtitle','jhu:jheHOSPtitle',
                                'jhu:jheHOSPPRIVATEtitle','jhu:jheSOMtitle',
                                'jhu:jheADHOCtitle','jhu:jheAPLtitle',
                                'jhu:jheACHtitle','jhu:jheHCGHtitle',
                                'jhu:jheMSOtitle','jhu:jheKKItitle',
                                'jhu:jheINTRASTAFFtitle','jhu:jheHHMItitle',
                                'jhu:jheSIBLEYtitle','jhu:jheSUBURBANtitle',
                                'jhu:jheSingaporetitle',
                            )
                        )
                   )
                )
            ),
         );
    }
}
