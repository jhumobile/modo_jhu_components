<?php

/*
 * Copyright © 2010 - 2014 Modo Labs Inc. All rights reserved.
 *
 * The license governing the contents of this file is located in the LICENSE
 * file located at the root directory of this distribution. If the LICENSE file
 * is missing, please contact sales@modolabs.com.
 *
 */

/**
 * @ingroup DataParser
 *
 * @brief A class to handle the parsing of external XML data.
 *
 * ### Initialization arguments:
 *
 * + __simpleMode__ (_boolean_)
 *     + Whether or not to run in simple mode.
 *     + Simple mode is typically faster than regular mode, but `multiValueElements` and `removeNamespaces` are not supported.
 *     + Defaults to `false`.
 *
 * + __multiValueElements__ (_array_)
 *     + An ordered array of element names which appear more than once inside a single parent element.
 *     + Only used if `simpleMode` is `false`.
 *     + Defaults to an empty array.
 *
 * + __removeNamespaces__ (_boolean_)
 *     + Whether or not to remove namespaces from element names.
 *     + Used when namespaces are not consistent.
 *     + Only used if `simpleMode` is `false`
 *     + Defaults to `false`.
 *
 * @see XMLDataParser
 */

class JHUWDDXPeopleParser extends KGODataParser
{
    protected $multiValueElements = array();
    protected $simpleMode = false;
    protected $lowercaseTags = true;
    protected $lowercaseAttributes = true;
    protected $removeNamespaces = false;
    protected $personClass = 'KGOPerson';
    protected $sortFields = array(KGOPerson::LASTNAME_ATTRIBUTE, KGOPerson::FIRSTNAME_ATTRIBUTE);
    protected $fieldMap;

    protected function init($args) {
        parent::init($args);
    }

    public function parseData($data) {
        //dont process empty or invalid records
        if (empty($data) || strpos($data,'var') == 0
            || strpos($data,'<number>0.0</number>') > 0
            || (strpos($data,'ERRORFLAG') > 0
                && strpos($data,"<var name='ERRORFLAG'><string>0</string>")==0) 
            )
            return null;

        try {
            $wddxArray = wddx_deserialize($data);
        } catch (Exception $e) {
            return null;
        }
        
        if (count($wddxArray))
        {
           if ((!isset($wddxArray['RECORDCOUNT']) || $wddxArray['RECORDCOUNT'] == 0) && !isset($wddxArray['cn']))
              return null;
           if (!isset($wddxArray['COLLECTION']) && isset($wddxArray['cn']))
              $wddxArray = array('COLLECTION' => array($wddxArray),'RECORDCOUNT' => 1);
        }
        else
          return null;


        foreach ($wddxArray['COLLECTION'] as $index => $person)
        {
          if (!isset($person['sn']))
              $person['sn'] = substr($person['cn'],strrpos($person['cn'],' ')+1);

          if (!isset($person['fn']) && !isset($person['displayname']) && !isset($person['givenname']))
              $person['fn'] = substr($person['cn'],strpos($person['cn'],' ')+1);
        }
        $this->setTotalItems($wddxArray['RECORDCOUNT'] ?: 1);
        for( $i=0 ; $i<($wddxArray['RECORDCOUNT'] ?: 1) ; $i++ )
        {
          $key=array_search('n/a',$wddxArray['COLLECTION'][$i]);
          while($key!==false)
          {
           unset($wddxArray['COLLECTION'][$i][$key]);
           $key=array_search('n/a',$wddxArray['COLLECTION'][$i]);
          }
        }
        return $wddxArray;
    }
}
