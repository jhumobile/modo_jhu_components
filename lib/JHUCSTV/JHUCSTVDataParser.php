<?php

/*
 * Copyright © 2010 - 2015 Modo Labs Inc. All rights reserved.
 *
 * The license governing the contents of this file is located in the LICENSE
 * file located at the root directory of this distribution. If the LICENSE file
 * is missing, please contact sales@modolabs.com.
 *
 */

/**
 * @brief Data parser for CSTV sport schedule.
 *
 * This class exists for efficiency purposes.  It filters events by sport pre-parsing.
 *
 */
class JHUCSTVDataParser extends KGOSimpleXMLDataParser
{
    protected $simpleMode = true;
    protected $sportId = null;

    /* Subclasseed */
    public function init($args) {
        parent::init($args);

        if (!isset($args['sportId'])) {
            throw new KGOConfigurationException('CSTV schedule feed missing sportId feed parameter');
        }
        $this->sportId = $args['sportId'];
    }

    /* Subclasseed */
    public function parseData($data) {
        $data = parent::parseData($data);

        // Filter events by sport here to avoid parsing all events
        $filtered = array();
        foreach ($data['event'] as $eventData) {
            if (isset($eventData['sport']) &&
                ($eventData['sport'] == $this->sportId || $this->sportId = 'allsports')) 
            {
                foreach($eventData as $key => $value) {
                    if (is_array($value) && !count($value)) {
                        $eventData[$key] = null;
                    }
                }
                $filtered[] = $eventData;
            }
        }
        $data['event'] = $filtered;

        return $data;
    }
}
