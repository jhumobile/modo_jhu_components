<?php

/*
 * Copyright © 2010 - 2015 Modo Labs Inc. All rights reserved.
 *
 * The license governing the contents of this file is located in the LICENSE
 * file located at the root directory of this distribution. If the LICENSE file
 * is missing, please contact sales@modolabs.com.
 *
 */

/**
 * @brief Data retriever for CSTV sport schedule.
 *
 *
 *
 */
class JHUCSTVDataRetriever extends KGOURLDataRetriever
{
    protected static $defaultParserClass = 'ModoCSTVDataParser';
    protected static $canPrefetchData = true;

    /**
     * ParseMap for events.
     *
     */
    public static function eventsParseMap() {
        return array(
            'debug' => true,
            'class' => 'ModoCSTVCalendarEvent',
            'key' => 'event',
            'array' => true,
            'attributes' => array(
                KGODataObject::ID_ATTRIBUTE => '@attributes.id',
                KGOCalendarEvent::LOCATION_ATTRIBUTE => 'location',
                ModoCSTVCalendarEvent::AUDIO_ARCHIVE_URL_ATTRIBUTE => 'audio_archive_url',
                ModoCSTVCalendarEvent::AUDIO_CURRENT_URL_ATTRIBUTE => 'audio_current_url',
                ModoCSTVCalendarEvent::AUDIO_DESCRIPTION_ATTRIBUTE => 'audio_description',
                ModoCSTVCalendarEvent::AUDIO_LIVE_URL_ATTRIBUTE => 'audio_live_url',
                ModoCSTVCalendarEvent::CSTV_DATE_ATTRIBUTE => 'event_date',
                ModoCSTVCalendarEvent::CSTV_TIME_ATTRIBUTE => 'time',
                ModoCSTVCalendarEvent::CSTV_TIMEZONE_ATTRIBUTE => 'time_zone',
                ModoCSTVCalendarEvent::CSTV_YEAR_ATTRIBUTE => 'year',
                ModoCSTVCalendarEvent::EVENT_TYPE_ATTRIBUTE => 'event_type',
                ModoCSTVCalendarEvent::EXTRA_INFO_ATTRIBUTE => 'extra_info',
                ModoCSTVCalendarEvent::HOME_ATTRIBUTE => 'home',
                ModoCSTVCalendarEvent::HOME_VISITOR_ATTRIBUTE => 'home_visitor',
                ModoCSTVCalendarEvent::LIVESTATS_OAS_FANSLIVE_TITLE_ATTRIBUTE => 'livestats_oas_fanslive_title',
                ModoCSTVCalendarEvent::LIVESTATS_OAS_FANSLIVE_URL_ATTRIBUTE => 'livestats_oas_fanslive_url',
                ModoCSTVCalendarEvent::LIVESTATS_OTHER_TITLE_ATTRIBUTE => 'livestats_other_title',
                ModoCSTVCalendarEvent::LIVESTATS_OTHER_URL_ATTRIBUTE => 'livestats_other_url',
                ModoCSTVCalendarEvent::NOTES_ATTRIBUTE => 'notes',
                ModoCSTVCalendarEvent::OPPONENT_ATTRIBUTE => 'opponent',
                ModoCSTVCalendarEvent::OUTCOME_SCORE_ATTRIBUTE => 'outcome_score',
                ModoCSTVCalendarEvent::QUOTES_ATTRIBUTE => 'quotes',
                ModoCSTVCalendarEvent::RADIO_STATION_INFO_ATTRIBUTE => 'radio',
                ModoCSTVCalendarEvent::RECAP_URL_ATTRIBUTE => 'recap',
                ModoCSTVCalendarEvent::SCHOOL_ATTRIBUTE => 'school',
                ModoCSTVCalendarEvent::SPECIAL_ATTRIBUTE => 'special',
                ModoCSTVCalendarEvent::SPORT_ID_ATTRIBUTE => 'sport',
                ModoCSTVCalendarEvent::SPORT_FULL_NAME_ATTRIBUTE => 'sport_fullname',
                ModoCSTVCalendarEvent::SPORT_SHORT_NAME_ATTRIBUTE => 'sport_shortname',
                ModoCSTVCalendarEvent::STATS_ATTRIBUTE => 'stats',
                ModoCSTVCalendarEvent::TICKET_URL_ATTRIBUTE => 'ticket_url',
                ModoCSTVCalendarEvent::TV_STATION_INFO_ATTRIBUTE => 'tv',
                ModoCSTVCalendarEvent::VENUE_ATTRIBUTE => 'venue',
                ModoCSTVCalendarEvent::VIDEO_ARCHIVE_URL_ATTRIBUTE => 'video_archive_url',
                ModoCSTVCalendarEvent::VIDEO_CURRENT_URL_ATTRIBUTE => 'video_current_url',
                ModoCSTVCalendarEvent::VIDEO_DESCRIPTION_ATTRIBUTE => 'video_description',
                ModoCSTVCalendarEvent::VIDEO_LIVE_URL_ATTRIBUTE => 'video_live_url',
                ModoCSTVCalendarEvent::VISITOR_ATTRIBUTE => 'visitor',
            ),
            'processors' => array(
                KGODataObject::TITLE_ATTRIBUTE => array(
                    array(
                        'class' => 'JHUCSTVTitleDataProcessor'
                    ),
                ),
                KGOCalendarEvent::START_ATTRIBUTE => array(
                    array(
                        'class' => 'ModoCSTVStartDateDataProcessor'
                    ),
                ),
                KGOCalendarEvent::END_ATTRIBUTE => array(
                    array(
                        'class' => 'ModoCSTVEndDateDataProcessor'
                    ),
                ),
                KGOCalendarEvent::ALLDAY_ATTRIBUTE => array(
                    array(
                        'class' => 'ModoCSTVAllDayDataProcessor'
                    ),
                ),
                ModoCSTVCalendarEvent::TBA_ATTRIBUTE => array(
                    array(
                        'class' => 'ModoCSTVTBADataProcessor'
                    ),
                ),
            ),
        );
    }

    /* Subclassed */
    public function getData(&$response = null) {
        $this->setParseMap(self::eventsParseMap());
        return parent::getData($response);
    }
}
