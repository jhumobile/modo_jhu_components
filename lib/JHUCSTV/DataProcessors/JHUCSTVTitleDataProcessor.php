<?php

/*
 * Copyright © 2010 - 2015 Modo Labs Inc. All rights reserved.
 *
 * The license governing the contents of this file is located in the LICENSE
 * file located at the root directory of this distribution. If the LICENSE file
 * is missing, please contact sales@modolabs.com.
 *
 */

/**
 * @ingroup DataProcessor
 *
 * @brief Generate the title of a CSTV event
 */

class JHUCSTVTitleDataProcessor extends KGODataProcessor
{
    protected function processValue($value, $object = null) {
        $school = $object->getAttribute(ModoCSTVCalendarEvent::SCHOOL_ATTRIBUTE);
        $opponent = $object->getAttribute(ModoCSTVCalendarEvent::OPPONENT_ATTRIBUTE);
        $score = $object->getAttribute(ModoCSTVCalendarEvent::OUTCOME_SCORE_ATTRIBUTE);
        $sport = $object->getAttribute(ModoCSTVCalendarEvent::SPORT_SHORT_NAME_ATTRIBUTE);

        $title = '';
        switch ($object->getAttribute(ModoCSTVCalendarEvent::HOME_VISITOR_ATTRIBUTE)) {
            case 'H':
                $title = "$opponent @ $school";
                break;

            case 'V':
                $title = "$school @ $opponent";
                break;

            default:
                $title =  "$school vs. $opponent";
        }

        if (strlen($sport)) {
            $title = $sport.", ".$title;
        }

        if (strlen($score)) {
            $title .= " ($score)";
        }

        return $title;
    }

    protected function canProcessValue($value, $object = null) {
        return ($object instanceOf ModoCSTVCalendarEvent);
    }
}
