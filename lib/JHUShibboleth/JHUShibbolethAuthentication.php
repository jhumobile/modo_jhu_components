<?php

class JHUShibbolethAuthentication extends KGOIndirectAuthenticationAuthority
{
    protected static $userClass = 'ModoShibbolethUser';
    protected $fieldMap = array();
    protected $shibbolethAttributes = array();

    private function _getCurrentUser() {

        $user = $this->createUser();

        if (isset($_SERVER['REMOTE_USER'])) {
            $user->setId($_SERVER['REMOTE_USER']);

            if ( ($field = $this->getField('email')) && isset($_SERVER[$field])) {
                $user->setEmail($_SERVER[$field]);
            }

            if ( ($field = $this->getField('fullname')) && isset($_SERVER[$field])) {
                $user->setFullName($_SERVER[$field]);
            }

            if ( ($field = $this->getField('firstname')) && isset($_SERVER[$field])) {
                $user->setFirstName($_SERVER[$field]);
            }

            if ( ($field = $this->getField('lastname')) && isset($_SERVER[$field])) {
                $user->setLastName($_SERVER[$field]);
            }

            foreach ($this->shibbolethAttributes as $attribute) {
                if (isset($_SERVER[$attribute])) {
                    $user->setAttribute($attribute, $_SERVER[$attribute]);
                }
            }

            return $user;
        }

        return null;
    }

    protected function auth($options) {

        if ($user = $this->_getCurrentUser()) {
            return new KGOAuthResult(true, null, KGOAuthResult::AUTH_OK, array('user'=>$user));
        }

		return new KGOAuthResult(false, null, KGOAuthResult::AUTH_FAILED);
    }

    public function getUser($login) {
        $user = $this->_getCurrentUser();

        if ($user && $user->getId() == $login) {
            return $user;
        }

        return false;
    }

    private function getField($field) {
        return isset($this->fieldMap[$field]) ? $this->fieldMap[$field] : null;
    }

    public function init($args) {
        parent::init($args);

        // set field map using SHIB_XXX_FIELD = "" maps to $_SERVER values
        foreach ($args as $arg=>$value) {
            if (preg_match("/^shib_(email|firstname|lastname|fullname)_field$/", strtolower($arg), $bits)) {
                $key = strtolower($bits[1]);
                $this->fieldMap[$key] = $value;
            }
        }

        if (isset($args['attributes']) && is_array($args['attributes'])) {
            $this->shibbolethAttributes = $args['attributes'];
        }
    }
}
